#include <MsTimer2.h>

typedef struct Fish {
  char image_path[50];
  float x;
  float y;
  int direction;
} Fish;

Fish fishes[10];
int nFishes;

Food foods[300];
int nFoods;

/* 친구 분 코드 */
void 물고기_사료들_출력() {
  for (int i = 0; i < nFoods; i++) {
    사료_한조각_출력(foods[i]);
  }
}

void 물고기들_출력() {
  for (int i = 0; i < nFishes; i++) {
    물고기_한마리_출력(fishes[i]);
  }
}

void 물고기_화면_갱신() {
  배경화면_출력();

  물고기_사료들_위치_계산();
  물고기_사료들_출력();

  물고기들_이동_계산();
  물고기들_출력();
}


/* 공동 코드 */
void 어항에_사료_넣기() {
  int index = nFood;
  foods[index].x = 랜덤위치();
  foods[index].y = 맨꼭대기();
  nFoods++;
}


/* 형래님 코드 */
int 영상찍기() {
  int sum_r, sum_g, sum_b;  // rgb 평균을 구하기 위한 임시 변수
  int count;                // rgb 평균을 구하기 위한 임시 변수
  
  for (int i = 0; i < 240; i++) {
    for (int j = 0; j < 320; j++) {
      int pixel = cam.read_pixel(); // 카메라로 부터 pixel data 읽기
      int r, g, b = ~~~~;   // pixel data를 r, g, b 값으로 변환 (RGB565 -> RGB888)

      // (30, 30) ~ (210, 290) 범위의 사각형에 대하여
      if ((i >= 30 || i < 210) && (j >= 30 || j < 290)) {
        sum_r += r;
        sum_g += g;
        sum_b += b;
        count++;
      }
    }
  }

  // 평균 구하기
  int mean_r, mean_g, mean_b;
  mean_r = sum_r / count;
  mean_g = sum_g / count;
  mean_b = sum_b / count;

  // 색상 유추하기
  // RGB -> HSV 변환
  double h; // 색조. 단위: 각도 (범위: 0~360)
  double s; // 채도. 단위: 퍼센트 (범위: 0~1)
  double v; // 밝기. 단위: 퍼센트 (범위: 0~1)
  if (h >= 80 && h <= 140) {  // 초록색 계열
    return 10000; // 만원이다!
  }
  if (h >= 176 && h <= 260) { // 파란색 계열
    return 1000;  // 천원이다!
  }
  if (h >= 34 && h <= 68) { // 노란색 계열
    return 5000;  // 오천원이다!
  }

  // 그 외엔
  return 0;  // 돈이 아니다!
}

void 영상찍고_사료주기() {
  int money = 영상찍기();

  int amount = 0;
  if (money == 1000) {
    amount = 1;
  }
  else if (money == 5000) {
    amount = 6;
  }
  else if (money == 10000) {
    amount = 13;
  }
  else if (money == 50000) {
    amount = 70;
  }
  else {
    // 예외!!! 돈을 인식 못함.
  }
  
  for (int i = 0; i < amount; i++) {
    어항에_사료_넣기();
  }
}

bool 지폐_들어왔나() {
  int value = 초음파센서값();
  if (value < 지폐인식거리) {
    return true;
  }
  else {
    return false;
  }
}

void 모터_작동() {
  analogWrite(motorPin, 20);
}

void 모터_끄기() {
  analogWrite(motorPin, 0);
}

void 라이트_켜기() {
  digitalWrite(ledPin, HIGH);
}

void 라이트_끄기() {
  digitalWrite(ledPin, LOW);
}

void setup() {
  // put your setup code here, to run once:

  MsTimer2::set(10, 물고기_화면_갱신); // 10ms period
  MsTimer2::start();
}

void loop() {
  if (지폐_들어왔나()) { // 초음파센서로 감지
    모터_작동();     // 지폐를 빨아들임
    라이트_켜기();   // 라이트를 미리 켜서 카메라를 적응시킨다.
    delay(500);     // 지폐가 어느정도 들어올 때 까지 영상찍기를 대기.
    
    영상찍고_사료주기();  // 어느정도 들어왔을테니, 찍어본다.
    라이트_끄기();   // 다 찍었으니, 라이트 끔
    delay(500);     // 지폐가 다 들어올 때 까지 모터를 굴림.
    
    모터_끄기();     // 지폐 빨아들이기 중지
  }
}